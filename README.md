
<!-- README.md is generated from README.Rmd. Please edit that file -->

# stars.ncdf

<!-- badges: start -->
<!-- badges: end -->

> This package is currently under development. Expect backwards
> incompatible changes! MR and feature requests (please open an issue)
> are very welcome.

The goal of `stars.ncdf` is to provide utilities to write
multidimensional and/or multivariable NetCDFs from stars objects, insert
slices into already existing NetCDF files etc.

## Installation

You can install the development version of `stars.ncdf` like so:

``` r
#install.packages("remotes")
remotes::install_gitlab(repo = "kmayer/stars.ncdf", host = "gitlab.geosphere.at")
```

## Usage

### Write multidimensional (often spatiotemporal) and multi-variable NetCDF

`stars::write_stars()` uses GDAL to write datasets, therefore its
limited to a single variable as well as spatial dimensions. A third
dimension is written as “band”, which is inconvenient if the dimension
is supposed to be encoded as time.

`stars.ncdf::write_stars_ncdf()` is a function which should work just as
easy, but with mutlidimensional and multi-variable datasets.

Let’s demonstrate the package functionality with a `stars` object
provided with the [`stars`](https://r-spatial.github.io/stars/) package.

``` r
library(stars)
#> Loading required package: abind
#> Warning: package 'abind' was built under R version 4.3.1
#> Loading required package: sf
#> Linking to GEOS 3.11.1, GDAL 3.6.2, PROJ 9.1.1; sf_use_s2() is TRUE
library(stars.ncdf)

dat <- read_stars(system.file("nc/bcsd_obs_1999.nc", package = "stars"))
#> pr, tas,

dat # print stars object
#> stars object with 3 dimensions and 2 attributes
#> attribute(s):
#>                 Min.   1st Qu.   Median      Mean   3rd Qu.      Max. NA's
#> pr [mm/m]  0.5900000 56.139999 81.88000 101.26433 121.07250 848.54999 7116
#> tas [C]   -0.4209678  8.898887 15.65763  15.48932  21.77979  29.38581 7116
#> dimension(s):
#>      from to offset  delta  refsys                    values x/y
#> x       1 81    -85  0.125      NA                      NULL [x]
#> y       1 33  37.12 -0.125      NA                      NULL [y]
#> time    1 12     NA     NA POSIXct 1999-01-31,...,1999-12-31
```

This stars object has a time dimension, and two variables. Writing with
the stars interface is not possible in a single file, writing a custom
output routine (e.g. with `ncdf4` or `tidync`) is cumbersome.

Such an object can be written directly with `write_stars_ncdf()`.

``` r
write_stars_ncdf(dat, "output.nc")
```

However, it is good practice to make sure that further important
information is included.

### Projection

For example, `dat` has no CRS assigned. The link to the dataset
documentation in the helpfile of this dataset reveals that its in
latlon, so lets assign EPSG:4326.

``` r
st_crs(dat) <- 4326

st_crs(dat) # print crs
#> Coordinate Reference System:
#>   User input: EPSG:4326 
#>   wkt:
#> GEOGCRS["WGS 84",
#>     ENSEMBLE["World Geodetic System 1984 ensemble",
#>         MEMBER["World Geodetic System 1984 (Transit)"],
#>         MEMBER["World Geodetic System 1984 (G730)"],
#>         MEMBER["World Geodetic System 1984 (G873)"],
#>         MEMBER["World Geodetic System 1984 (G1150)"],
#>         MEMBER["World Geodetic System 1984 (G1674)"],
#>         MEMBER["World Geodetic System 1984 (G1762)"],
#>         MEMBER["World Geodetic System 1984 (G2139)"],
#>         ELLIPSOID["WGS 84",6378137,298.257223563,
#>             LENGTHUNIT["metre",1]],
#>         ENSEMBLEACCURACY[2.0]],
#>     PRIMEM["Greenwich",0,
#>         ANGLEUNIT["degree",0.0174532925199433]],
#>     CS[ellipsoidal,2],
#>         AXIS["geodetic latitude (Lat)",north,
#>             ORDER[1],
#>             ANGLEUNIT["degree",0.0174532925199433]],
#>         AXIS["geodetic longitude (Lon)",east,
#>             ORDER[2],
#>             ANGLEUNIT["degree",0.0174532925199433]],
#>     USAGE[
#>         SCOPE["Horizontal component of 3D system."],
#>         AREA["World."],
#>         BBOX[-90,-180,90,180]],
#>     ID["EPSG",4326]]
```

This information is used by the output function to add important
attributes to the dimension specification in the output file and add CRS
information.

## Global and variable attributes

Global attributes can be set using usual R object attributes on the
stars object. However, a helper function makes it really convenient to
do so by using a list:

``` r
# global attributes

gloattr <- list(
    title = "Example dataset",
    institution = "GeoSphere Austria, Vienna, Austria"
)

dat <- st_add_global_attr(dat, gloattr)
```

Attributes to the variables can be set as usual R object attributes on
the contained data arrays. Also, here a helper function is provided.
Attributes must be provided as a names list (using variable names):

``` r
# variable attributes

varattr <- list(
    pr = list(
        long_name = "precipitation",
        short_name = "precipitation_amount"
    ),
    tas = list(
        long_name = "temperature",
        short_name = "air_temperature"
    )
)

dat <- st_add_variables_attr(dat, varattr)
```

Now the dataset can be written:

``` r
write_stars_ncdf(dat, "output.nc")
```

`write_stars_ncdf` tries to populate as much metadata as possible using
the information provided with the object:

``` bash
$ ncdump -h output.nc 
netcdf output {
dimensions:
        x = 81 ;
        y = 33 ;
        time = 12 ;
variables:
        double x(x) ;
                x:units = "degree" ;
                x:standard_name = "longitude" ;
        double y(y) ;
                y:units = "degree" ;
                y:standard_name = "latitude" ;
        double time(time) ;
                time:units = "days since 1960-01-01 00:00:00" ;
                time:long_name = "time" ;
                time:standard_name = "time" ;
                time:axis = "T" ;
        double pr(time, y, x) ;
                pr:units = "mm m-1" ;
                pr:_FillValue = NaN ;
                pr:long_name = "precipitation" ;
                pr:short_name = "precipitation_amount" ;
                pr:grid_mapping = "crs" ;
        double tas(time, y, x) ;
                tas:units = "C" ;
                tas:_FillValue = NaN ;
                tas:long_name = "temperature" ;
                tas:short_name = "air_temperature" ;
                tas:grid_mapping = "crs" ;
        char crs ;
                crs:semi_major_axis = 6378137. ;
                crs:inverse_flattening = 298.257223563 ;
                crs:reference_ellipsoid_name = "WGS 84" ;
                crs:horizontal_datum_name = "World_Geodetic_System_1984" ;
                crs:grid_mapping_name = "latitude_longitude" ;
                crs:geographic_crs_name = "WGS 84" ;
                crs:longitude_of_prime_meridian = 0. ;
                crs:crs_wkt = "GEOGCS[\"WGS 84\",DATUM[\"WGS_1984\",SPHEROID[\"WGS 84\",6378137,298.257223563]],PRIMEM[\"Greenwich\",0],UNIT[\"degree\",0.0174532925199433,AUTHORITY[\"EPSG\",\"9122\"]],AXIS[\"Latitude\",NORTH],AXIS[\"Longitude\",EAST],AUTHORITY[\"EPSG\",\"4326\"]]" ;

// global attributes:
                :title = "Example dataset" ;
                :institution = "GeoSphere Austria, Vienna, Austria" ;
}
```

## Packing

An easy way to save storage is to transform data using a scale value and
offset and save it as integer - so called packing. Scale value and
offset must be added to the variable attributes, so software can do the
retransformation when loading a NetCDF file.

`st_pack()` is a convenience function to do all in one step and can be
applied before writing data to disk with `write_stars_ncdf()`.

If no additional arguments are given, scale factor and offset are set
for each variable for packing with maximal precision (at 32bit).

``` r
dat_packed <- st_pack(dat)
```

However, these can also be set for each variable to certain values:

``` r
dat_packed2 <- st_pack(dat, 
        pack = list(
                pr = list(
                        scale_factor = 0.001, 
                        add_offset = 0
                )
        ))

write_stars_ncdf(dat_packed2, "output_packed.nc")
```

Be aware, that the objects `dat_packed` and `dat_packed2` already hold
the transformed (packed) data.

# Time bounds

Coordinate bounds could in general be specified for any coordinate, but
is probably most often set for the time dimension. For aggregated
measures (i.e. precipitation sums, mean temperature etc.), this
clarifies what is labeled with the time coordinates (e.g. whether the
time coordinates specifiy the left or right end, the center of a time
interval, or any other point relative to the interval).

With `stars.ncdf` one can easily set time bounds by providing a
data.frame with two colums for the left and right end of the intervals,
as well as many rows as the time coordinate is long.  
`st_time_bnds` can be used to view set time bounds, but also to set
them. Alternatively one can use `st_set_time_bnds()`. These are stored
as an attribute to the values of the time dimension - please be aware
that this is a feature only implemented within `stars.ncdf` and not part
of the `stars` data model.

`dat` holds monthly aggregates of meteorogical data, obviously the time
coordinate specifies the right end of each monthly interval. We
therefore construct a dataframe with the first day of the month as the
left end of the interval, and the time coordinates as the right end.
(This is strictly not CF-compliant, as contiguous intervals are sharing
a coordinate by this standard. You would need to specify
`left = lubridate::floor_date(time_coord, "month") - lubridate::days(1)`),
to do so, but we keep things simple for this quickstart guide)

``` r
time_coord <- st_time(dat)
time_bnds <- data.frame(left = lubridate::floor_date(time_coord, "month"), right = time_coord)
st_time_bnds(dat) <- time_bnds

st_time_bnds(dat) # show time bounds
#>          left      right
#> 1  1999-01-01 1999-01-31
#> 2  1999-02-01 1999-02-28
#> 3  1999-03-01 1999-03-31
#> 4  1999-04-01 1999-04-30
#> 5  1999-05-01 1999-05-31
#> 6  1999-06-01 1999-06-30
#> 7  1999-07-01 1999-07-31
#> 8  1999-08-01 1999-08-31
#> 9  1999-09-01 1999-09-30
#> 10 1999-10-01 1999-10-31
#> 11 1999-11-01 1999-11-30
#> 12 1999-12-01 1999-12-31
```

Often, intervals are of the same size. The argument `each` in
`st_set_time_bnds()` lets you specify a vector of length two to modify
(add to) each of the values in the time coordinate. However, always
check the output, as some things won’t work out as expected:

``` r
test <- st_set_time_bnds(dat, each = c(months(-1), months(0)))
st_time_bnds(test)
#>          left      right
#> 1  1998-12-31 1999-01-31
#> 2  1999-01-28 1999-02-28
#> 3        <NA> 1999-03-31
#> 4  1999-03-30 1999-04-30
#> 5        <NA> 1999-05-31
#> 6  1999-05-30 1999-06-30
#> 7        <NA> 1999-07-31
#> 8  1999-07-31 1999-08-31
#> 9  1999-08-30 1999-09-30
#> 10       <NA> 1999-10-31
#> 11 1999-10-30 1999-11-30
#> 12       <NA> 1999-12-31
```

Here an `NA` value is appearing whenever a certain day of the month does
not exist the month before.

`each` is also accepting functions instead, providing more flexibility.

``` r
test2 <-  st_set_time_bnds(dat, each = c(function(x) lubridate::floor_date(x, "month"), function(x) x))
st_time_bnds(test2)
#>          left      right
#> 1  1999-01-01 1999-01-31
#> 2  1999-02-01 1999-02-28
#> 3  1999-03-01 1999-03-31
#> 4  1999-04-01 1999-04-30
#> 5  1999-05-01 1999-05-31
#> 6  1999-06-01 1999-06-30
#> 7  1999-07-01 1999-07-31
#> 8  1999-08-01 1999-08-31
#> 9  1999-09-01 1999-09-30
#> 10 1999-10-01 1999-10-31
#> 11 1999-11-01 1999-11-30
#> 12 1999-12-01 1999-12-31
```

Bounds set within the stars object are written to the NetCDF file.

``` r
write_stars_ncdf(dat, "output_time_bnds.nc")
```

``` bash
$ ncdump output_time_bnds.nc
netcdf output_time_bnds {
dimensions:
        x = 81 ;
        y = 33 ;
        time = 12 ;
        nv = 2 ;
variables:
        double x(x) ;
                x:units = "degree" ;
                x:standard_name = "longitude" ;
        double y(y) ;
                y:units = "degree" ;
                y:standard_name = "latitude" ;
        double time(time) ;
                time:units = "days since 1960-01-01 00:00:00" ;
                time:long_name = "time" ;
                time:standard_name = "time" ;
                time:axis = "T" ;
                **time:bounds = "time_bnds" ;**
        double pr(time, y, x) ;
                pr:units = "mm m-1" ;
                pr:_FillValue = NaN ;
                pr:long_name = "precipitation" ;
                pr:short_name = "precipitation_amount" ;
                pr:grid_mapping = "crs" ;
.
.
.
        char crs ;
                crs:semi_major_axis = 6378137. ;
                crs:inverse_flattening = 298.257223563 ;
                crs:reference_ellipsoid_name = "WGS 84" ;
                crs:horizontal_datum_name = "World_Geodetic_System_1984_ensemble" ;
                crs:grid_mapping_name = "latitude_longitude" ;
                crs:geographic_crs_name = "WGS 84" ;
                crs:longitude_of_prime_meridian = 0. ;
                crs:crs_wkt = "GEOGCS[\"WGS 84\",DATUM[\"WGS_1984\",SPHEROID[\"WGS 84\",6378137,298.257223563]],PRIMEM[\"Greenwich\",0],UNIT[\"degree\",0.0174532925199433,AUTHORITY[\"EPSG\",\"9122\"]],AXIS[\"Latitude\",NORTH],AXIS[\"Longitude\",EAST],AUTHORITY[\"EPSG\",\"4326\"]]" ;
        double time_bnds(time, nv) ;

// global attributes:
                :title = "Example dataset" ;
                :institution = "GeoSphere Austria, Vienna, Austria" ;

.
.
.

 time_bnds =
  14245, 14275,
  14276, 14303,
  14304, 14334,
  14335, 14364,
  14365, 14395,
  14396, 14425,
  14426, 14456,
  14457, 14487,
  14488, 14517,
  14518, 14548,
  14549, 14578,
  14579, 14609 ;
}
```
