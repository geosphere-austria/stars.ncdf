FROM rocker/verse:latest
RUN apt-get update && apt-get install -y  gdal-bin libgdal-dev libgeos-dev libgeos++-dev libnetcdf-dev libproj-dev libssl-dev libudunits2-dev && rm -rf /var/lib/apt/lists/*
RUN mkdir -p /usr/local/lib/R/etc/ /usr/lib/R/etc/
RUN echo "options(repos = c(CRAN = 'https://cran.rstudio.com/'), download.file.method = 'libcurl', Ncpus = 4)" | tee /usr/local/lib/R/etc/Rprofile.site | tee /usr/lib/R/etc/Rprofile.site
RUN R -e 'install.packages("remotes")'
RUN Rscript -e 'remotes::install_version("sf",upgrade="never", version = "1.0-7")'
RUN Rscript -e 'remotes::install_version("units",upgrade="never", version = "0.8-0")'
RUN Rscript -e 'remotes::install_version("stars",upgrade="never", version = "0.5-5")'
RUN Rscript -e 'remotes::install_version("purrr",upgrade="never", version = "0.3.4")'
RUN Rscript -e 'remotes::install_version("proj4",upgrade="never", version = "1.0-11")'
RUN Rscript -e 'remotes::install_version("ncdf4",upgrade="never", version = "1.19")'
RUN Rscript -e 'remotes::install_version("lubridate",upgrade="never", version = "1.8.0")'
RUN Rscript -e 'remotes::install_version("jsonlite",upgrade="never", version = "1.8.0")'
RUN mkdir /build_zone
ADD . /build_zone
WORKDIR /build_zone
RUN R -e 'remotes::install_local(upgrade="never")'
RUN rm -rf /build_zone
