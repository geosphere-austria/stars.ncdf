
time_since <- function(datetimes, since = "1960-01-01 00:00:00") {
  stopifnot(length(since) == 1)
  lubridate::as_datetime(datetimes) - lubridate::as_datetime(since)
}

has_crs <- function(x) {
  !is.na(sf::st_crs(x))
}

get_units <- purrr::possibly(units, otherwise = "")

nc_prepare_crs <- function(x, grid_mapping = wkt2cf(sf::st_crs(x))) {
  # TODO: issue #1
  list(crs = 
    c(
      grid_mapping,
      crs_wkt = sf::st_crs(x)$Wkt
    )
    )
}

prec_lut <- c("double" = "double", "integer" = "integer", "logical" = "integer", "character" = "char")
na_lut <- list("double" = NA_real_, "integer" = NA_integer_, "logical" = NULL, "character" = NULL)

nc_prepare_variables <- function(x, prec = prec_lut[sapply(x, typeof)], missval = na_lut[sapply(x, typeof)]) {
  out <- st_variables_attr(x)
  for (nm in names(out)) {
    idx <- which(names(out) == nm)
    # add reference to crs
    if(has_crs(x)) {out[[nm]]$grid_mapping <- "crs"}
    # add precision
    out[[nm]]$prec <- prec[[idx]]
    # missval
    out[[nm]]$missval <- missval[[idx]]
    # units
    out[[nm]]$units <- purrr::possibly(units::deparse_unit, otherwise = "")(x[[nm]])
    # long_name
    if(!"long_name" %in% names(out[[nm]])) {
      out[[nm]]$long_name <- nm
    }
    # values
    out[[nm]]$values <- x[[nm]]
  }
out
}

nc_prepare_dimensions <- function(x, time_since = "1960-01-01 00:00:00") {
  
  # spatial dimensions
  all_dims <- names(stars::st_dimensions(x))
  spatial <- st_spatialdims(x)
  values <- stats::setNames(purrr::map(spatial,
    ~stars::st_get_dimension_values(x, .x)), nm = spatial)

  if(has_crs(x)){
    units <- stats::setNames(purrr::map(spatial, ~sf::st_crs(x)$units_gdal), 
      nm = spatial)
  } else {
    units <- rep("", length(spatial))
  }

  out <- purrr::transpose(list(values = values, units = units))
  
  # TODO: include more cases, such as rotated pole, other dimension names etc
  # if projected and standard dimension names used, add attributes
  if(has_crs(x)) {
    if((!sf::st_crs(x)$IsGeographic) && all(names(out) %in% spatial)) {
      for (dm in spatial) {
        idx <- which(spatial == dm)
        xy <- c("x", "y")[[idx]]
        out[[dm]][["long_name"]] <- paste0(xy, " coordinate of projection")
        out[[dm]][["standard_name"]] <- paste0("projection_", xy, "_coordinate")
        out[[dm]][["axis"]] <- toupper(xy)
      }
    } else { # or classic lon lat
      out[[spatial[[1]]]][["standard_name"]] <- "longitude"
      out[[spatial[[2]]]][["standard_name"]] <- "latitude"
    }
  }

  # temporal dimension 
  time_nm <- st_timedim(x)
  if(length(time_nm) > 0) {
    timediff <- time_since(st_time(x), since = time_since)
    out[[time_nm]][["values"]] <- as.numeric(timediff)
    out[[time_nm]][["units"]] <- paste(attr(timediff, "units"), "since", as.character(time_since))
    out[[time_nm]][["long_name"]] <- "time"
    out[[time_nm]][["standard_name"]] <- "time"
    out[[time_nm]][["axis"]] <- "T"

    if (!is.null(st_time_bnds(x))) {
      out[[time_nm]][["bounds"]] <- "time_bnds"
    }
  }

  # other dimensions
  other_dims <- all_dims[!all_dims %in% c(spatial, time_nm)]
  if(length(other_dims > 0)) {
    for (dm in other_dims) {
      out[[dm]][["long_name"]] <- dm
      out[[dm]][["values"]] <- stars::st_get_dimension_values(x, dm)
      out[[dm]][["units"]] <- get_units(out[[dm]][["values"]])
    }
  }

  out
}

nc_prepare_globalattr <- function(x) {
  st_global_attr(x)
}


nc_prepare_lonlat <- function(x) {
    values <- st_lonlat_matrizes(x)
    grid_mapping <- rep("crs", 2)
    purrr::transpose(list(values = values, grid_mapping = grid_mapping))
}
